<!doctype html>

<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo THEMEROOT ?>/favicon.ico">
    <?php wp_head(); ?>

    <style>
        #flat-select .flats-table .flats-table__filters .rangeslider-min .irs-line-mid, #flat-select .flats-table .flats-table__filters .rangeslider-min .irs-line-left, #flat-select .flats-table .flats-table__filters .rangeslider-min .irs-line-right, #flat-select .flats-table .flats-table__filters .rangeslider-min .irs-bar, #flat-select .flats-table .flats-table__filters .rangeslider-min .irs-bar-edge, #flat-select .flats-table .flats-table__filters .rangeslider-min .irs-slider {
            background-image: url('<?php echo ASSETS ?>/js/vendor/ion.rangeslider/img/sprite-skin-nice-min.png');
        }

        #flats {
            background-image: url("<?php echo ASSETS ?>/images/section-5-bg.jpg");
        }

        #about_complex .about_complex__video:before {
            background-image: url("<?php echo ASSETS ?>/images/section-2-bg.jpg");
        }

        #feedback-form {
            background-image: url("<?php echo ASSETS ?>/images/section-3-bg.jpg");
        }

        #calc {
            background-image: url("<?php echo ASSETS ?>/images/section-4-bg.jpg");
        }

        #rewards {
            background-image: url("<?php echo ASSETS ?>/images/section-6-bg.jpg");
            background-position: center top;
        }

        #flat-select .corpus3 .corpus3-fllor16 {
            background-image: url("<?php echo ASSETS ?>/images/floor-hover.png");
        }

        #flat-select .detail-window .scheme-wrap .floor-item {
            background-image: url("<?php echo ASSETS ?>/images/flat-hover.png");
        }

        #flat-select .flat-list-container {
            background-image: url("<?php echo ASSETS ?>/images/section-8-bg.jpg");
        }

        ul.benefits li {
            background-image: url("<?php echo ASSETS ?>/images/check-circle.png");
        }
    </style>
</head>

<body>

<!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<div id="App">

<?php 
    $phone = get_field('phone', 'option');
    if($phone) {
        $normalized_phone = preg_filter('/[\D+]/i', '', $footer_phone);
    }
?>

<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="header__logo-wrapper">
                <a href="#index"><img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php the_field('company_name', 'option'); ?>" class="header__logo"></a>
            </div>
            <div class="header__menu-wrapper">
                <ul class="menu" id="mainmenu">
                    <?php if ( have_rows('nav', 'option') ) : ?>
                        <?php while( have_rows('nav', 'option') ) : the_row(); ?>
                    
                            <li><a href="<?php the_sub_field('section_id'); ?>"><?php the_sub_field('text'); ?></a></li>
                    
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
                <a href="#" class="header__menu-mob-trigger">
                    <div id="nav-burger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <a class="header__link" href="http://www.mois1.ru/objects/non-residential-property/documentation/" target="_blank">Документы</a>
            </div>
            <div class="header__phone-btn">
                <?php if($phone): ?>
                <a href="tel:<?php echo $normalized_phone; ?>" class="header__phone">
                    <i class="fas fa-phone-volume"></i> <span><?php echo $phone; ?></span>
                </a>
                <?php endif; ?>
                <div class="header__btn-wrapper">
                    <a href="#callme" data-fancybox data-src="#callme" class="btn btn-primary btn-small btn-radius header__request-btn">Заказать звонок</a>
                    <a class="header__link" href="http://www.mois1.ru/objects/non-residential-property/documentation/" target="_blank">Документы</a>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="menu-mob">
    <ul>
        <?php if ( have_rows('nav', 'option') ) : ?>
            <?php while( have_rows('nav', 'option') ) : the_row(); ?>
        
                <li><a href="<?php the_sub_field('section_id'); ?>"><?php the_sub_field('text'); ?></a></li>
        
            <?php endwhile; ?>
        <?php endif; ?>
    </ul>
</div>
$(document).ready(function() {
	$('#fullpage').fullpage({
		anchors: ['index','make-choice','complex','about_flats','our-rewards', 'photos','our-gallery','credit','questions','map'],
		animateAnchor: true,
		responsiveWidth: 1024,
		verticalCentered: false,
		normalScrollElements: "#flat-select .scrollbar-inner"
	});

	$('#nav-burger').click(function(event){
		event.preventDefault();
		$(this).toggleClass('open');
		$(".menu-mob").toggleClass('active');
		$("body").toggleClass('fixed');
	});
	$('.menu-mob > ul > li > a').click(function(event){
		$('#nav-burger.open').removeClass('open');
		$(".menu-mob.active").removeClass('active');
		$("body.fixed").removeClass('fixed');
	});

	$('.carousel-container').slick({
		slidesToShow: 2,
		slidesToScroll: 2,
		dots: false,
		arrows: false,
		infinite: true,
		adaptiveHeight: true,
		centerMode: true,
		centerPadding: '0', 
		responsive: [{
			breakpoint: 1024,
			settings:{
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});

	$('.carousel-container').slick('slickSetOption', 'slidesToScroll', 2, true);

	$("#gallery .tab.active .gallery-container").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		infinite: true,
		adaptiveHeight: true,
		centerMode: true,
		variableWidth: true,
	});

	$(".gallery-tabs a").click(function(event) {
		event.preventDefault();

		$('#gallery .tab.active .gallery-container').slick('unslick');

		$(".gallery-tabs a.active").removeClass("active");
		$(this).addClass("active");
		var target = $(this).data("target");
		$("#gallery .tab.active").removeClass("active");
		$("#"+target).addClass("active");

		
		$("#gallery .tab.active .gallery-container").slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			infinite: true,
			adaptiveHeight: true,
			centerMode: true,
			variableWidth: true,
		});
	});

	$("#gallery .gallery-nav-toleft").click(function(event) {
		event.preventDefault();
		$("#gallery .tab.active .gallery-container").slick('slickPrev');
	});
	$("#gallery .gallery-nav-toright").click(function(event) {
		event.preventDefault();
		$("#gallery .tab.active .gallery-container").slick('slickNext');
	});

	$('.carousel-container .carousel__toleft').click(function(event) {
		event.preventDefault();
		$('.carousel-container').slick('slickPrev');
	});
	$('.carousel-container .carousel__toright').click(function(event) {
		event.preventDefault();
		$('.carousel-container').slick('slickNext');
	});

	$("#calc-summ").ionRangeSlider({
		min: 500000,
    	max: 7000000,
    	from: 2000000,
    	step: 100000,
    	hide_min_max: true,
    	hide_from_to: true,
    	onChange: function(data){
    		updateCalcValue(data);
    		calcResult();
    	}
	});

	$("#calc-period").ionRangeSlider({
		min: 12,
    	max: 300,
    	from: 240,
    	step: 1,
    	hide_min_max: true,
    	hide_from_to: true,
    	onChange: function(data){
    		updateCalcValue(data);
    		calcResult();
    	}
	});

	$("#calc-percent").ionRangeSlider({
		min: 5,
    	max: 13,
    	from: 9,
    	hide_min_max: true,
    	hide_from_to: true,
    	step: 0.5,
    	onChange: function(data){
    		updateCalcValue(data);
    		calcResult();
    	}
	});

	/*$(".floor-item").hover(function() {
		var corpus = $(this).parents(".corpus-item");
		var top = $(this).position().top;
		var tooltip = $(".tooltip",corpus);
		$(tooltip).addClass("show").css("top", top - $(tooltip).height()*2 - 30);
		$(".corpus-value", tooltip).text($(this).data("corpus"));
		$(".floor-value", tooltip).text($(this).data("floor"));
	}, function() {
		var corpus = $(this).parents(".corpus-item");
		$(".tooltip",corpus).removeClass("show").css("top", 0);
	});*/

	// $(".corpus-svg a").click(function(event) {
	// 	event.preventDefault();
	// 	$(".detail-window").addClass("opened");
	// 	$(".detail-window .floor-value").text($(this).attr("xlink:href"));
	// });
	// $(".corpus-svg path").hover(function() {
	// 	var corpus = "К "+$(this).parents("svg").data("corpus")+", ";
	// 	if($(this).parent("a").length > 0){
	// 		var tooltip_text = corpus+" этаж "+$(this).parent("a").attr("xlink:href");
	// 	}else{
	// 		var tooltip_text = "этаж не в продаже";
	// 	}
	// 	$(".flat-tooltip").text('');
	// 	$(".flat-tooltip").text(tooltip_text);
	// 	$(".flat-tooltip").css("top", $(this).position().top);
	// 	$(".flat-tooltip").css("left", $(this).position().left);
	// 	$(".flat-tooltip").addClass('show');
	// }, function() {
	// 	$(".flat-tooltip").removeClass('show');
	// });

	// $(".detail-window__close-btn").click(function(event) {
	// 	event.preventDefault();
	// 	$(".detail-window.opened").removeClass('opened');
	// });

	// $(".detail-window .floor-arrow").click(function(event) {
	// 	event.preventDefault();
	// 	/* Act on the event */
	// });

	// $(".detail-window .flat-item, .scheme-wrap svg path").hover(function() {
	// 	$(".flat-tooltip").text('');
	// 	$(".flat-tooltip").text($(this).data("tooltip"));
	// 	$(".flat-tooltip").css("top", $(this).position().top);
	// 	$(".flat-tooltip").css("left", $(this).position().left);
	// 	$(".flat-tooltip").addClass('show');
	// }, function() {
	// 	$(".flat-tooltip").removeClass('show');
	// });
	
	// $(".detail-window .flat-item").click(function(event) {
	// 	event.preventDefault();
	// 	$(".detail-window").addClass("flat-active");
	// });

	// $(".detail-window .detail-flat__close-btn").click(function(event) {
	// 	event.preventDefault();
	// 	$(".detail-window").removeClass("flat-active");
	// 	$(".detail-window").removeClass("opened");
	// });
	// $(".detail-window .btn-show-floor").click(function(event) {
	// 	event.preventDefault();
	// 	$(".detail-window").removeClass("flat-active");
	// });

	// $(".flats-tabs a").click(function(event) {
	// 	event.preventDefault();
	// 	$(".flats-tabs a.active").removeClass("active");
	// 	$(this).addClass("active");
	// 	var target = $(this).data("target");
	// 	$("#flat-select .tab.active").removeClass("active");
	// 	$("#"+target).addClass("active");
	// });

	$("#tab-list .flat-sheme-btn").click(function(event) {
		event.preventDefault();
		if($(document).width() >= 1024){
			$(".detail-window").addClass("opened flat-active");
		}else{
			$.fancybox.open('<div class="message"><img src="/images/flat-scheme.jpg"></div>');
		}
	});

	$("input[name='filter_price']").ionRangeSlider({
		type: "double",
		min: 2500000,
    	max: 12200000,
    	from: 4500000,
    	to: 10200000,
    	step: 100000,
    	hide_min_max: true,
    	hide_from_to: true,
    	onChange: function(data){
    		flatFilterUpdateValues(data);
    	}
	});

	$("input[name='filter_area']").ionRangeSlider({
		type: "double",
		min: 35,
    	max: 300,
    	from: 58,
    	to: 250,
    	step: 1,
    	hide_min_max: true,
    	hide_from_to: true,
    	onChange: function(data){
    		flatFilterUpdateValues(data);
    	}
	});

	$("input[name='filter_floor']").ionRangeSlider({
		type: "double",
		min: 1,
    	max: 20,
    	from: 2,
    	to: 19,
    	step: 1,
    	hide_min_max: true,
    	hide_from_to: true,
    	onChange: function(data){
    		flatFilterUpdateValues(data);
    	}
	});

	// $('.ae-select-content').text($('.dropdown-list > li.selected').text());
	// $('.dropdown-list > li').click(function() {
    // 	$('.ae-select-content').text($(this).text());
    // 	$('.dropdown-menu > li').removeClass('selected');
    // 	$(this).addClass('selected');
	// });

	// $('.ae-dropdown').click(function() {
    // 	$('.dropdown-list').toggleClass('ae-hide');
	// });
});

function updateCalcValue(data){
	var container = $(data.input).parents(".calc-param");
	$(".calc-value span",container).text(data.from_pretty);
}

function calcResult(){
	var summ = $("#calc-summ").val();
	var period = $("#calc-period").val();
	var percent = $("#calc-percent").val();
	var result = Math.round((summ*percent/12/100) / (1-Math.pow(1 + percent/12/100, -period)));
	var pretify = result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	$(".calc-result .result span").text(pretify);
}

function flatFilterUpdateValues(data){
	var container = $(data.input).parents(".filter-item");
	$(".filter-item__from",container).text(data.from_pretty);
	$(".filter-item__to",container).text(data.to_pretty);
}
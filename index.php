
<?php get_header(); ?>

<div id="fullpage">
    
    <div class="section" id="welcome" style="background-image:url(<?php echo get_field('hero_bg')['url']; ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="complex-name">
                        <img src="<?php echo ASSETS ?>/images/complex-name-icon.png" class="complex-name__icon">
                        ЖК на Ленинском проспекте<br>
                        <span class="complex-name__subname">Жилая и коммерческая недвижимость</span>
                    </div>
                    <h1><?php the_field('hero_heading'); ?></h1>
                    <div class="lead"><?php the_field('hero_text'); ?></div>
                    <a href="#complex" class="btn btn-transparent btn-small btn-radius"><i class="fas fa-chevron-down"></i>&nbsp;&nbsp;Узнать больше</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="section" id="flat-select">
        <div :style="{top: tooltip.top + 'px', left: tooltip.left + 'px'}" v-show="tooltip.visible" class="flat-tooltip" :class="tooltip.class">{{ tooltip.content }}</div>
        <div :class="{'detail-window': true, 'opened': floorDetailsOpened, 'flat-active': apartmentDetailsOpened}">
            <div class="detail-window__head">
                <div class="floor-mode">
                    Выберите квартиру
                </div>
                <div class="flat-mode">
                    {{ floors.byId[activeFloor] ? floors.byId[activeFloor].code : '16' }} этаж
                    <a @click="showFloorView()" href="#" class="btn btn-small btn-transparent btn-radius btn-show-floor">Показать этаж</a>

                </div>
                <a href="#" @click="closeFloorDetails()" class="detail-window__close-btn"></a>
            </div>
            <div class="scheme-wrap">
                <loader v-if="floorDetailsLoading"></loader>
                <div class="floor-scheme-wrap">
                    <img :key="activeFloor" v-if="floors.byId[activeFloor]" :src="floors.byId[activeFloor].img" class="floor-scheme-img" alt="">
                    <svg :view-box.camel="'0 0 '+floors.byId[activeFloor].width+' '+floors.byId[activeFloor].height" v-if="floors.byId[activeFloor] && floors.byId[activeFloor].flats" height="720" version="1.1" width="720" xmlns="http://www.w3.org/2000/svg">
                        <a @click.prevent="openApartmentDetails(floors.byId[activeFloor].id, flat.url)" v-for="flat in floors.byId[activeFloor].coordinates" :class="{'soldout': !apartments.byId[floors.byId[activeFloor].flats[flat.url].ID]}" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path :data-flat-url="flat.url" :data-avail="flat.avail" @mouseenter="handleHousePathMouseEnter($event, apartments.byId[floors.byId[activeFloor].flats[flat.url].ID] ? flat.name : 'Квартира не в продаже')" @mouseleave="handleHousePathMouseLeave()" :d="flat.path" stroke-width="0" opacity="0.5"></path>
                        </a>
                    </svg>
                </div>
            </div>
            <div v-if="floors.byId[activeFloor]" class="floor-up-down">
                Этаж 
                <div class="floor-value">{{ String(floors.byId[activeFloor].code).replace(/\d+-/, '')  }}</div>
                <a :disabled="nextFloor == false" @click="changeFloor(nextFloor)" href="#" class="floor-arrow floor-up"><i class="fas fa-long-arrow-alt-up"></i></a>
                <a :disabled="prevFloor == false" @click="changeFloor(prevFloor)" href="#" class="floor-arrow floor-down"><i class="fas fa-long-arrow-alt-down"></i></a>
            </div>
            <div class="detail-flat">
                <loader v-if="apartmentDetailsLoading"></loader>
                <a @click="closeFloorDetails(), closeApartmentDetails()" href="#" class="detail-flat__close-btn"></a>
                <div v-if="activeApartment.id" class="flat-info">
                    <div class="flat-info__content">
                        <div class="flat-info__head">
                            Квартира №{{activeApartment.acf.apartment_info.number}}
                        </div>
                        <table class="flat-info__parameters">
                            <tr><td>Корпус</td><td class="flat-info__value">{{activeApartment.acf.apartment_info.houseNumber}}</td></tr>
                            <tr><td>Номер</td><td class="flat-info__value">{{activeApartment.acf.apartment_info.number}}</td></tr>
                            <tr><td>Этаж</td><td class="flat-info__value">{{activeApartment.acf.apartment_info.floor}}</td></tr>
                            <tr><td>Комнат</td><td class="flat-info__value">{{activeApartment.acf.apartment_info.rooms}}</td></tr>
                            <tr><td>Площадь</td><td class="flat-info__value">{{activeApartment.acf.apartment_info.area}}</td></tr>
                        </table>
                        <div class="flat-info__bottom">
                            Стоимость
                            <div class="flat-info__price">{{activeApartment.acf.apartment_info.price.toLocaleString('ru')}} руб.</div>
                            <div v-if="activeApartment.acf.apartment_info.price !== activeApartment.acf.apartment_info.price_special">
                                Стоимость со скидкой 
                                <div class="flat-info__price">{{activeApartment.acf.apartment_info.price_special.toLocaleString('ru')}} руб.</div>
                            </div>
                            <a href="#callme" data-fancybox data-src="#callme" class="btn btn-block btn-lg btn-primary">Забронировать</a>
                        </div>
                    </div>
                </div>
                <div class="flat-scheme">
                    <img v-if="activeApartment.img" :src="activeApartment.img" class="scheme-img" alt="">
                </div>
            </div>
        </div>
    
        <div class="tabs-content">
            <div :class="{'tab': true, 'active': activeTab === 'tab-interactive'}" id="tab-interactive">
                <div class="main-container">
                    <div class="img-wrapper" style="position:relative;">

                        <svg viewBox="0 0 2098.286 655.967" class="corpus-svg" height="945" version="1.1" width="498" xmlns="http://www.w3.org/2000/svg">
                            <!-- <a @click.prevent="openHouseDetails(house.url)" xmlns:xlink="http://www.w3.org/1999/xlink" :xlink:href="house.url">
                                
                            </a> -->
                            
                            <a class="corpus-svg-link" @click.prevent="openHouseDetails(141);" xlink:href="1" xmlns:xlink="http:///www.w3.org/1999/xlink">
                                <path @mouseenter="handleHousePathMouseEnter($event, 'ЖК на Ленинском Проспекте К.1')" @mouseleave="handleHousePathMouseLeave()" class="corpus-svg-path" d="M264.844,1296.57V961.957l4.189-1.4h6.051v2.792L308.6,965.68v-3.723l2.793-.931,2.327,0.465,15.36,1.4,3.724-1.4,8.844,1.862,15.825,1.4h2.793v-1.4h7.913l4.189,4.188V969.4h5.585v-2.792l7.447,0.465,2.328,2.792,9.309,0.931h1.4v-2.327l5.586,0.465,6.516,1.4,1.4,1.4,10.706,0.465,5.12,4.189,1.862,2.792v313.2l-1.4-1.86-6.516-2.79-3.258-2.33-0.931-4.19-1.862-3.26a22.49,22.49,0,0,1-2.793,0l-2.327,2.33-2.793,2.33h-3.723l-1.862-4.66a25.012,25.012,0,0,0-.931-2.79c-0.468-1.07-4.189-3.26-4.189-3.26l-3.724.47-4.189,1.4s-3.075-.56-4.189-0.93-3.258-1.4-3.258-1.4a2.757,2.757,0,0,0-2.328-1.4,10.948,10.948,0,0,0-3.258.93l-2.792.47a3.939,3.939,0,0,1-1.4-1.4,45.26,45.26,0,0,0-2.327-4.19l-2.793.94,0.466,4.65,0.465,4.19-0.931,2.32-3.258.94-5.585-3.73-3.259-3.26-1.4-1.86-1.862.47-3.723,1.86-3.259-.93-0.931.46-2.327,2.33s-0.077-.22-2.327.47-2.327.93-2.327,0.93l1.4,1.86v1.4l-0.931,2.32,0.931,2.33-1.862,2.33-2.327,2.79-6.051.46-3.258-.46-4.655-.93-2.793-2.79h-2.327l-0.465,2.32-0.466,1.86H308.6l-2.327,1.87-2.327,1.86-1.862-.47s-1.783-2.39-2.327-2.32a16.173,16.173,0,0,0-1.862.46l-0.931.47-1.862.93v4.19a2.218,2.218,0,0,1-.931,1.86c-0.984.6-3.258,1.86-3.258,1.86l-1.862,2.79,0.466,2.33-5.586,4.65-2.793.47-4.654-3.73-3.258-4.65-3.258-2.33-2.328-1.39Z" transform="translate(-264.844 -672.813)"/>
                            </a>
                            <a class="corpus-svg-link" @click.prevent="openHouseDetails(72);" xlink:href="1" xmlns:xlink="http:///www.w3.org/1999/xlink">
                                <path @mouseenter="handleHousePathMouseEnter($event, 'ЖК на Ленинском Проспекте К.2')" @mouseleave="handleHousePathMouseLeave()" class="corpus-svg-path" d="M430.08,1290.99v-377.9l6.051-1.4,4.655,0.931v2.792l36.77,1.861v-3.723l2.793-.93,19.549,1.4,4.655-1.862,7.913,3.258,19.083,0.931,5.12,1.861,2.793,3.723h6.051V921l7.912,0.465,1.862,1.861,18.153,0.931v-2.792h4.189l8.378,2.792,0.466,2.327h6.982l3.723,6.05,2.327,1.862,3.259,6.515,6.516,0.931V953.58l-1.862.93v4.654l1.862,0.931v5.585l-1.4,1.4v7.912l1.4,0.465v6.05l-1.862.931v8.377h1.4v6.981l-1.4.465v7.913l1.862,0.46v6.98l-1.862.47v7.91l1.862,0.47v6.51l-1.862.93v7.91l1.862,0.47v6.52l-1.862.93v7.91l1.862,0.46v6.52l-1.862.46v8.38l1.862,0.47v6.05l-1.862,1.39v7.45l1.862,0.46v6.52l-1.862.93v7.91l1.862,0.47v6.98l-1.862.46v7.92l1.862,0.46v6.98l-1.862.47v8.37l1.862,0.47v5.58l-1.862.93v8.38l1.862,0.47-0.465,6.51-1.4.93v7.91l1.862,0.47v6.05l-1.862.93v8.38l1.4,0.46v6.05l-1.4.93v8.38l1.862,0.47v6.05l-1.862.93v8.37l1.862,0.47v6.05l-1.862.93v8.38l1.862,0.46v6.52l-1.862.46v8.38l1.862,0.47v6.05l-1.862.46v8.84h1.862v6.98h-1.862v33.98l-1.862-2.33-0.931-1.86-3.723,1.4s-1.143-1.73-.931-2.33-0.466-4.65-.466-4.65l-2.327,1.39-4.189.93-1.862.93-0.931-4.19v-3.25l-1.4-.93-2.793.46-1.861,1.86-1.862,1.86-3.259-1.86-3.258-2.32-4.654.93-3.724,2.32s-0.7-2.78-.931-3.25-1.4-3.73-1.4-3.73l-2.793-1.86-2.327,1.86-1.4,2.8-2.792-.94s-3.154-2.26-3.724-1.86a28.382,28.382,0,0,1-2.793,1.4l-0.93,1.86-2.328,1.4-3.258.46s-2.57.04-3.258,0a9.662,9.662,0,0,0-2.793,0,11.129,11.129,0,0,1-2.793,0l-3.723-1.39-0.931-3.26-3.258-.93s-4.01-1.08-5.12-.93a24.158,24.158,0,0,0-4.655.46c-1.706.5-1.632,1.41-3.258,1.86a6.727,6.727,0,0,1-3.258,0,13.321,13.321,0,0,0-2.328,0,15.9,15.9,0,0,0-4.654,1.87c-0.783.58-1.394-1.16-2.793,1.39a15.271,15.271,0,0,0-1.4,2.79l-0.466,1.87,1.862,1.39-1.862,2.33-0.465,2.33,0.931,1.86,0.465,5.12-2.327.93-2.793-.47-1.4,1.4-0.466,4.19-2.327,1.39-0.465,4.19,1.4,2.33v3.72l-0.931,2.79,5.586,0.93v2.33a24.452,24.452,0,0,1-3.259,1.86,27.4,27.4,0,0,1-4.189,0s-3.224-1.59-3.723-2.32a13.008,13.008,0,0,0-1.862-1.87l-3.724-.46s-3.044-1.14-3.723-2.33-2.17-4.13-2.328-4.65-2.327-4.19-2.327-4.19l-3.258-2.79-7.913-3.26-5.12-2.79-5.585-1.4-2.793-3.26,0.931-4.65-1.862-3.72-1.862-1.87-4.189-1.86h-2.327l-1.4,2.33-1.861,1.86Z" transform="translate(-264.844 -672.813)"/>
                            </a>
                            <a class="corpus-svg-link" @click.prevent="openHouseDetails(60);" xlink:href="1" xmlns:xlink="http:///www.w3.org/1999/xlink">
                                <path @mouseenter="handleHousePathMouseEnter($event, 'ЖК на Ленинском Проспекте К.3')" @mouseleave="handleHousePathMouseLeave()" class="corpus-svg-path" d="M628.364,1296.57l0.465-60.5-5.585-.47v-7.44l1.861-.93v-8.38l-2.327-.46v-7.45l2.327-.47v-9.3l-2.792-.47v-7.45l2.792-.46v-9.31l-2.327-.93v-7.91l1.862-.47v-8.37l-1.862-.93-0.465-7.45,2.792-.93v-9.31l-1.861-1.4v-6.51l2.327-.47v-9.3l-3.258-.93v-7.45l3.258-.47v-9.3l-3.258-.47v-7.45l2.327-.46v-8.38l-2.327-.46v-7.91l2.327-1.4v-8.38h-1.862v-8.37l1.862-.94v-8.37h-1.862v-8.38l2.327-.47v-8.84h-2.327v-7.91l2.793-.46-0.931-9.31-1.862-1.4v-7.44l2.327-.94v-7.44l-2.327-1.4v-7.909l2.327-.931-0.465-8.377-2.327-.465v-7.912h2.792V969.4l-2.792-.931v-6.981l2.792-.93v-8.377l-2.327-.931v-7.446l1.862-.931v-9.308h-1.862v-8.842l2.327-.466v-7.446l-2.327-1.4v-7.912l1.862-1.4v-34.9l3.724-1.4v-6.516l3.723-.465-0.465-13.5,6.051-1.862,6.982,0.466,0.465,2.792,38.633,3.723V849.8l5.12-.465,18.618,1.861,6.051-2.327,9.309,3.258,17.687,1.4,6.051,1.862v4.654l39.564,2.327v-2.327l5.12,0.465,6.982,3.258v3.257l1.861,1.4v3.257l3.258,0.931,5.121,1.862-0.466,8.377,6.982,3.257v11.17l-5.585,1.861v4.189l5.585,1.861v6.981l-6.051,1.4v6.981l5.585,2.327v5.585l-5.119,2.327-0.466,6.05,6.051,3.257V946.6L808.96,948v6.515l5.12,1.862v7.446l-5.12,1.4v7.447l6.516,1.4v6.981l-6.981,1.4v6.515l6.05,2.327-0.465,6.516-5.12,1.861,0.466,7.907,5.585,1.4v6.05l-6.051,2.33v6.51l6.051,1.86v7.45l-6.051.93v6.98l6.516,1.86v7.92l-6.516.93v6.98l5.585,1.39v7.92l-6.05,1.39v6.52l6.516,1.86v6.98l-5.585.93v7.91l5.119,1.4v6.98l-5.585,1.39v7.45l6.051,1.86v6.98l-5.585.93v7.92l5.119,1.39v6.98l-5.119.93v7.91l4.654,1.4v6.05l-5.585,1.4v8.84h6.981v7.91l-6.981.93v8.38l6.516,1.4v6.05l-5.585,1.86v8.37l5.585,0.47v6.98l-6.051.93v8.84l5.585,0.93v6.98l-5.119.47v9.31l5.119,0.46v6.52l-5.119,1.86v28.39l-5.586-.93-0.931,5.12-0.931,2.32-0.931,2.8-4.654,2.79a5.371,5.371,0,0,1-3.258.46c-1.382-.43,0-0.34-4.19-1.86s-5.585-3.26-5.585-3.26,1.638-4.22-.465-4.65a28.028,28.028,0,0,1-5.12-1.86l5.585-4.65-0.465-6.05-6.982-6.05-6.517-.94-6.051-2.32-6.981-2.79-7.448-.47-5.585-2.33L741,1242.59l-6.982-1.87-7.447,1.4-5.586,2.79-9.774.47-6.517-2.33-7.913,2.79-4.654-2.32-3.724-3.73-6.982.47-5.12,1.39-3.258,1.87-4.654,4.18-2.793,5.12,2.327,3.73,1.4,4.19-1.862,5.11-5.12-.46-5.12-3.26-2.793,3.26,0.466,4.65-3.259,1.86-0.931,4.66-0.931,3.26s-2.777,1.05-2.792,3.25a13.249,13.249,0,0,0,.465,3.73l-4.189,2.32-2.793-.93-4.189,6.05-2.327,3.26Z" transform="translate(-264.844 -672.813)"/>
                            </a>
                            <a class="corpus-svg-link" @click.prevent="openHouseDetails(175);" xlink:href="1" xmlns:xlink="http:///www.w3.org/1999/xlink">
                                <path @mouseenter="handleHousePathMouseEnter($event, 'ЖК на Ленинском Проспекте К.4')" @mouseleave="handleHousePathMouseLeave()" class="corpus-svg-path" d="M899.189,1235.14l-0.465-257.826-9.309-2.792-0.466-51.193-6.516-104.316,68.956-28.388,30.72-16.22,86.111-35.835,58.65-24.269,49.33-16.289,48.41-25.2,113.18,55.52v32.112l-18.23,4.654v5.584l-47.47,19.081v4.189l-12.1,4.188-0.47,457.01-3.26,2.33-0.46-7.45-4.19.93-5.12,2.33-0.47,4.65h-5.58l-3.73-3.72-3.72-1.4-4.66.93-5.12-2.32-3.72-3.73-3.26-3.72-7.44-2.33s-2.47-3.4-3.73-3.25-6.98,1.86-6.98,1.86h-3.26l-5.12-5.12s-2.66-1.95-4.65-1.4-8.38,1.86-8.38,1.86l-6.98-.46-4.19,1.4-9.31.46-1.86,4.65-6.05,2.8-5.12-3.26,2.32-4.19,3.26-3.26v-6.05l-5.58-7.44-6.99-7.91-7.44,2.79-3.73-2.33s-1.65-2.87-3.72-2.33-6.52,1.4-6.52,1.4h-5.58a4.5,4.5,0,0,0-4.66-1.86c-3.37.44-3.94-.67-6.51,0.93s-2.32,2.14-4.66,2.33-6.51-1.87-6.51-1.87l3.72-4.65-2.33-5.12-7.44-2.33s-8.19-.64-10.71-0.46-6.52,3.26-6.52,3.26l-6.05,3.72-8.38,3.26-6.05,3.72-1.86,3.26,4.66,4.19,0.46,5.58-2.79,2.79-3.26.93-6.05-3.25-5.12-1.4-2.33,3.26-3.72,3.72-0.93,5.12s-0.02,4.52-1.4,6.05a43.887,43.887,0,0,0-4.65,6.05c-0.5,1.12,2.32,9.31,2.32,9.31l2.33,3.72-7.91,4.19h-4.19l-4.65-.47-5.124-.93-6.052,5.12h-6.05l-1.4,4.66s-4.2,2.29-5.12.93-4.189-6.52-4.189-6.52l-6.516-7.44-5.586-6.98-5.585-6.05s1.5-2.7-3.724-3.73-17.222-5.58-17.222-5.58-12.528-1.34-12.567-2.79-0.931-6.05-.931-6.05l-5.12-1.4-8.912,2.79-7.913,2.33-2.327,5.12Z" transform="translate(-264.844 -672.813)"/>
                            </a>
                            <a class="corpus-svg-link" :class="{'corpus-svg-link-sold': !isHouseAvaliable(210)}" @click.prevent="openHouseDetails(210);" xlink:href="1" xmlns:xlink="http:///www.w3.org/1999/xlink">
                                <path @mouseenter="handleHousePathMouseEnter($event, isHouseAvaliable(210) ? 'ЖК на Ленинском Проспекте К.5' : 'Дом не в продаже')" @mouseleave="handleHousePathMouseLeave()" class="corpus-svg-path" d="M1829.7,859.571l2.33,386.269,1.86,7.91,9.31-7.44-0.93-13.03-3.72-12.57,4.18-9.31,3.26-5.11,3.26-4.66,8.38-4.19,8.84,6.05,3.73,2.8,15.82-15.36,15.83-3.73,5.12,2.33,12.1,9.77,6.98,4.66,9.78,1.86,9.3,1.4,0.94,4.65s-0.38,2.33-1.4,6.52,3.26,7.91,3.26,7.91l8.84-.93,7.45,7.91,8.84,11.17,18.15,1.39,16.76-8.84,8.38,1.86,6.98,3.26,10.24-.93,4.66-3.26,2.32,4.66,3.26,6.05,4.19,3.72,3.26-12.57,2.79-4.19,20.02,1.4,1.86,5.12,4.19,3.72,11.17-2.32,21.87-1.4,19.09-3.26-0.93-7.44,7.44-6.52s3.3-4.26,4.19-1.86,2.72,10.25,4.19,9.77,7.91-5.58,7.91-5.58l0.47-6.98,3.72-4.19,2.8-8.84,10.24-10.71,7.91,5.12,1.86,7.45s5.52,2.03,5.59,3.72,1.86,4.65,1.86,4.65l5.58-9.77,9.31-11.63,7.45-7.91,10.71,0.46,6.51-4.65,11.17-6.52,13.97-7.44,18.61-6.52,14.9-.47,12.57-2.79a9.814,9.814,0,0,1,6.51,0,90.489,90.489,0,0,0,10.24,2.79s2.94,1.25,3.73,2.33,7.91,2.33,7.91,2.33l12.57,0.93h9.31l9.3,1.4,3.26-.94v-375.1l-2.79-1.862L2238.84,753l-4.19-2.792-138.71,36.3-9.77-6.98-98.68,26.061,0.47,5.585-38.64,13.5-87.5,22.8v4.189Z" transform="translate(-264.844 -672.813)"/>
                            </a>
                            <a class="corpus-svg-link" :class="{'corpus-svg-link-sold': !isHouseAvaliable(210)}" @click.prevent="openHouseDetails(210);" xlink:href="1" xmlns:xlink="http:///www.w3.org/1999/xlink">
                                <path @mouseenter="handleHousePathMouseEnter($event, isHouseAvaliable(210) ? 'ЖК на Ленинском Проспекте К.5' : 'Дом не в продаже')" @mouseleave="handleHousePathMouseLeave()" class="corpus-svg-path" d="M1498.3,1196.05V825.133l21.41-7.447,0.46-4.654,72.62-24.2,46.54-19.08,16.29-3.724-0.46-2.792,70.75-22.8,6.98,2.792,54.46-18.15,7.44,3.723,35.84-12.1,76.34,44.677,0.46,74-46.08,11.635v5.119l-31.18,7.912,0.93,383.013-6.52,1.4-6.51,1.39-5.59-1.39-2.33-6.98-7.91-3.26-3.72,2.79-2.33,3.26-10.71,3.72-3.72,1.4-1.4,3.26-2.32,2.79-6.99-3.73-0.93,6.05v4.66l-4.19,7.91-6.05,6.52-10.24-4.19-7.44,1.86-6.52,2.33,0.93-9.78-15.82,2.79-8.38,6.05-3.26-6.98,4.19-10.23,4.19-6.99,3.26-7.44-4.19-6.05s-5.77-7.03-6.99-6.98-9.3,2.09-9.77.93-3.26-8.38-3.26-8.38l1.4-6.98a53.122,53.122,0,0,0-2.33-6.98,52.454,52.454,0,0,1-2.33-7.45l-12.56-3.72-4.19-6.05-5.12,5.59h-4.66l-4.19-3.73-5.12-2.32-6.05,1.86h-8.38l-1.86,5.12-0.93,5.58-12.57-.46-6.51-1.87-2.33,3.26-6.98,4.66-8.84,3.25-8.38-.46-4.66,3.26-9.77,1.39-6.98-1.86a11.33,11.33,0,0,0-5.59-1.86c-3.74-.39-10.7-0.93-10.7-0.93h-7.92l-6.98-2.79-10.24-.47-7.91-.93s-11.99-4.28-13.5-4.65-7.45-1.4-7.45-1.4Z" transform="translate(-264.844 -672.813)"/>
                            </a>
                            <a class="corpus-svg-link" :class="{'corpus-svg-link-sold': !isHouseAvaliable(210)}" @click.prevent="openHouseDetails(210);" xlink:href="1" xmlns:xlink="http:///www.w3.org/1999/xlink">
                                <path @mouseenter="handleHousePathMouseEnter($event, isHouseAvaliable(210) ? 'ЖК на Ленинском Проспекте К.5' : 'Дом не в продаже')" @mouseleave="handleHousePathMouseLeave()" class="corpus-svg-path" d="M1259.99,1251.89l-0.47-452.819,13.03-5.585v-3.723l49.81-20.477,36.3-19.081,52.6-16.288,54.92-22.408,8.38,2.257,24.67-9.238,84.25,39.627-0.47,48.4-63.3,20.942v4.654l-20.95,6.05V1197.44l-6.51,2.33-2.33,3.26-3.26,2.32-0.93,3.73-4.19,2.79-3.72.93-3.73.47s-0.42-.57-1.86.46-2.41.99-3.72,2.33a33.018,33.018,0,0,1-3.26,2.79l-2.79,3.72v11.17l-0.93,1.86-0.47,5.12v4.66l0.47,2.79-3.26,1.4-1.86,1.39a5.823,5.823,0,0,0-3.26,1.4,20.669,20.669,0,0,1-3.26,2.33s-1.7,2.77-2.79,4.18a27.884,27.884,0,0,1-3.26,3.26l-3.26,2.33-4.65-.47s-3.81-6.34-4.19-7.44-0.68-5.7-2.33-5.59-3.36-1.02-6.05,0-4.35.07-6.05-.93-6.52-3.26-6.52-3.26l-2.79-4.65-6.52-1.4-3.25,3.73a8.5,8.5,0,0,0-2.33.93,18.548,18.548,0,0,1-8.38,1.39,54.822,54.822,0,0,0-9.31.93s-3.03.51-4.19-.93a47.519,47.519,0,0,1-3.26-5.58s0.92-2.86-1.39-4.19-6.52-4.19-6.52-4.19l-7.45-2.79-12.1,2.33-3.72,3.72-3.73,1.4-7.44-4.66s-5.68-3.28-6.98-2.79-4.66-6.98-4.66-6.98l-4.19-3.72-8.84,3.72s-3.75-4.04-5.59-2.79a25.622,25.622,0,0,1-5.58,2.32l-5.12.47a74.757,74.757,0,0,0-7.45.93c-1.46.43-4.46,0.86-5.59,2.33s-5.58,4.65-5.58,4.65l-3.72,3.26-0.47,5.58-1.4,5.12Z" transform="translate(-264.844 -672.813)"/>
                            </a>
                        </svg>

                        <!-- <svg v-if="housesArray.length > 0" v-for="(house, index) in housesArray" :key="house.id" :id="'corpus'+(index+1)+'-svg'" class="corpus-svg" :data-corpus="house.code" height="945" version="1.1" width="498" xmlns="http://www.w3.org/2000/svg">
                            <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                            <a @click.prevent="openFloorDetails(house.id, floor.url)" v-for="floor in house.coordinates"  xmlns:xlink="http://www.w3.org/1999/xlink" :xlink:href="floor.number" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                                <path @mouseenter="handleHousePathMouseEnter($event, house.floors[floor.url] && house.floors[floor.url].FLATS_FOR_SALE.length > 0 ? ('к.'+(index+1)+', Этаж '+floor.number) : 'Этаж не в продаже')" @mouseleave="handleHousePathMouseLeave()" :fill="house.floors[floor.url] && house.floors[floor.url].FLATS_FOR_SALE.length > 0 ? '#ffffff' : '#ff3333'" stroke="#000000" :d="floor.path" stroke-width="0" opacity="0" :data-hasqtip="floor.number" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); cursor: pointer; opacity: 0;"></path>
                            </a>
                        </svg> -->

                        <img class="corpus-buildings" src="<?php echo ASSETS ?>/images/buildings.jpg" alt="">
                        <!-- <div class="corpus-item corpus1"></div>
                        <div class="corpus-item corpus2"></div>
                        <div class="corpus-item corpus3">
                            <a href="#" data-corpus="3" data-floor="16" class="floor-item corpus3-fllor16"></a>
                            <div class="tooltip">К <span class="corpus-value"></span>, <span class="floor-value"></span> этаж</div>
                        </div>
                        <div class="corpus-item corpus4"></div> -->
                    </div>
                </div>
            </div>
            <div :class="{'tab': true, 'active': activeTab === 'tab-house'}" class="tab-house">
                <button @click="showTab('tab-interactive');" class="house-back" v-if="!houseLoading">
                    <i class="fas fa-arrow-left"></i>
                    Назад к корпусам
                </button>
                <loader v-if="houseLoading"></loader>
                <div v-if="activeHouse" class="house">
                    <svg :id="'corpus'+(activeHouse.id)+'-svg'" class="house__svg" :data-corpus="activeHouse.code" height="945" version="1.1" width="498" xmlns="http://www.w3.org/2000/svg">
                        <a @click.prevent="openFloorDetails(activeHouse.id, floor.url)" v-if="activeHouse.coordinates" v-for="floor in activeHouse.coordinates"  xmlns:xlink="http://www.w3.org/1999/xlink" :xlink:href="floor.number" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                            <path 
                                @mouseenter="handleFloorPathMouseEnter($event, houseTooltipContent(activeHouse, floor))" 
                                @mouseleave="handleFloorPathMouseLeave()" :fill="isHouseFloorSold(activeHouse, floor) ? '#ffffff' : '#ff3333'" 
                                stroke="#000000" 
                                :d="floor.path" 
                                stroke-width="0" 
                                opacity="0" 
                                :data-hasqtip="floor.number" 
                                style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); cursor: pointer;"></path>
                        </a>
                    </svg>
                    <img class="responsive-img house__img" v-if="activeHouse.img" :src="activeHouse.img" alt="activeHouse.name">
                </div>
            </div>
            <div :class="{'tab': true, 'active': activeTab === 'tab-list'}" id="tab-list">
                <apartment-table :is-mobile="isMobile" :table-loading="apartmentTableLoading" :apartments="apartmentsArray" :visible="apartmentTableVisible" v-show="apartmentTableVisible"></apartment-table>
            </div>
        </div>
        <div class="flats-tabs">
            <div class="container">
                <a :class="{'active': activeTab === 'tab-interactive' || activeTab === 'tab-house'}" @click="showTab('tab-interactive'); hideApartemntTable();" href="#" data-target="tab-interactive">Выбрать квартиру</a>
                <a :class="{'active': activeTab === 'tab-list'}" @click="showTab('tab-list'); showApartmentTable();" href="#" data-target="tab-list">Подобрать по параметрам</a>
            </div>
        </div>

    </div>
    
    <div class="section" id="about_complex">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-12">
                    <h2><?php the_field('about_heading'); ?></h2>
                    <p class="lead"><?php the_field('about_subheading'); ?></p>
                    <div class="row">
                        
                        <?php if ( have_rows('about_advantages') ) : ?>
                            <?php while( have_rows('about_advantages') ) : the_row(); ?>
                        
                                <div class="col-md-6">
                                    <ul class="benefits">
                                        <li><?php the_sub_field('text'); ?></li>
                                    </ul>
                                </div>
                        
                            <?php endwhile; ?>
                        <?php endif; ?>
                        
                    </div>
                    <div class="about_complex__video about_complex__video-inline">
                        <div class="about_complex__video-content">
                            <a href="#video" data-fancybox data-src="#video" class="about_complex__btn"><i class="fas fa-play"></i></a>
                            Посмотрите короткое видео<br>о ЖК на Ленинском
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="highlite-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <div class="row">
                            
                            <?php if ( have_rows('about_advantages_bottom') ) : ?>
                            
                                <?php while( have_rows('about_advantages_bottom') ) : the_row(); ?>
                            
                                    <?php $advantage_icon = get_sub_field('icon'); ?>
                                    <div class="col col-12 col-md-6">
                                        <img src="<?php echo $advantage_icon['url']; ?>" class="d-lg-block" alt="<?php echo $advantage_icon['alt']; ?>">
                                        <?php the_sub_field('text'); ?>
                                    </div>
                            
                                <?php endwhile; ?>
                            
                            <?php endif; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about_complex__video d-none d-md-block">
            <div class="about_complex__video-content">
                <a href="#video" data-fancybox data-src="#video" class="about_complex__btn"><i class="fas fa-play"></i></a>
                Посмотрите короткое видео<br>о ЖК на Ленинском
            </div>
        </div>
    </div>

    <div class="section" id="flats">
        <div class="container">
            <h2><?php the_field('about_apartment_heading'); ?></h2>
            <div class="benefits-wrap">
                <div class="row">
                    
                    <?php if ( have_rows('about_apartment_advantages_right') ) : ?>
                        <?php $index = 1; ?>
                        <?php while( have_rows('about_apartment_advantages_right') ) : the_row(); ?>
                    
                            <?php $should_open_list = $index % 4 === 0 || $index === 1; ?>
                            <?php $should_close_list  = $index % 3 === 0 ?> 

                            <?php if($should_open_list): ?>
                            <div class="col-lg-6">
                                <ul class="benefits">
                            <?php endif; ?>

                                    <li><?php the_sub_field('text'); ?></li>

                            <?php if($should_close_list): ?>
                                </ul>
                            </div>
                            <?php endif; ?>

                        <?php $index++ ?> 
                        <?php endwhile; ?>
                    
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
        <div class="highlite-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="row">
                            
                            <?php if ( have_rows('about_apartment_advantages_left') ) : ?>
                            
                                <?php while( have_rows('about_apartment_advantages_left') ) : the_row(); ?>
                            
                                    <?php $about_apartment_icon = get_sub_field('icon'); ?>

                                    <div class="col-md-4">
                                        <img src="<?php echo $about_apartment_icon['url']; ?>" alt="<?php echo $about_apartment_icon['alt']; ?>">
                                        <?php the_sub_field('text'); ?>
                                    </div>
                                    
                                <?php endwhile; ?>
                            
                            <?php endif; ?>
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section" id="rewards">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7 col-sm-12">
                    <h2><?php the_field('rewards_heading'); ?></h2>
                </div>
            </div>
            <div class="rewards-wrap">
                <h3>Наши награды</h3>
                <ul class="docs-list">
                    
                    <?php if ( have_rows('rewards') ) : ?>
                    
                        <?php while( have_rows('rewards') ) : the_row(); ?>
                            <?php 
                                $reward_img = get_sub_field('img'); 
                                $preview_img = get_sub_field('preview');
                            ?>
                    
                            <li><a href="<?php echo $preview_img['url']; ?>" data-fancybox="rewards"><img src="<?php echo $reward_img['url']; ?>" class="docs-list__preview" alt=""></a><?php the_sub_field('text'); ?></li>
                            
                        <?php endwhile; ?>
                    
                    <?php endif; ?>
                        
                </ul>
                <div class="bottom-line">Узнайте больше о застройщике на <a target="_blank" href="http://www.mois1.ru">официальном сайте</a></div>
            </div>
        </div>
    </div>

    <div class="section" id="carousel">
        <div class="carousel-container">
            
            <?php if ( have_rows('advantages') ) : ?>

                <?php $index = 0; ?>
                <?php while( have_rows('advantages') ) : the_row(); ?>

                    <?php $adv_img = get_sub_field('img'); ?>
                    <?php $is_odd = $index % 2 != 0; ?>

                    <div>
                        <img class="<?php if($is_odd) echo 'right'; ?>" src="<?php echo $adv_img['url']; ?>">
                        <div class="caption <?php if(!$is_odd) echo 'bottom'; ?>">
                            <div class="caption__title"><?php the_sub_field('title'); ?></div>
                            <?php the_sub_field('text'); ?>
                            <a href="#" class="carousel__toleft"><i class="fas fa-arrow-left"></i></a>
                            <a href="#" class="carousel__toright"><i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>

                <?php $index++ ?> 
                <?php endwhile; ?>
            
            <?php endif; ?>
                
        </div>
    </div>

    <div class="section" id="gallery">
        <div class="tabs-content">
            <?php if ( have_rows('gallery_cats') ) : ?>
            
                <?php $gallery_cat_index = 0; ?>
                <?php while( have_rows('gallery_cats') ) : the_row(); ?>

                    <div class="tab <?php if($gallery_cat_index === 0) echo 'active'; ?>" id="gallery-corpus<?php echo $gallery_cat_index; ?>">
                        <div class="gallery-container">
                            <?php $gallery_slider = get_sub_field('slider'); ?>
                            <?php foreach($gallery_slider as $gallery_img): ?>
                                <div class="img-holder" style="background-image: url(<?php echo $gallery_img['url']; ?>);"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                <?php $gallery_cat_index++ ?>
                <?php endwhile; ?>
            
            <?php endif; ?>
        </div> 
                
        <div class="gallery-tabs">
            <div class="container">
                
                <?php if ( have_rows('gallery_cats') ) : ?>

                    <?php $gallery_cat_index = 0; ?>
                    <?php while( have_rows('gallery_cats') ) : the_row(); ?>
                
                        <a href="#" class="<?php if($gallery_cat_index === 0) echo 'active'; ?>" data-target="gallery-corpus<?php echo $gallery_cat_index; ?>"><?php the_sub_field('name'); ?></a>

                    <?php $gallery_cat_index++ ?>
                    <?php endwhile; ?>
                
                <?php endif; ?>
                
            </div>
        </div>
        <div class="gallery-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Галерея</h2>
                    </div>
                    <div class="col-md-9 text-lg-right text-xl-right text-md-right gallery-nav-wrap">
                        <a href="#callme" data-fancybox data-src="#callme" class="btn btn-transparent btn-small btn-radius request-btn"><i class="fas fa-pencil-alt"></i> Записаться на просмотр</a>
                        <a href="#" class="gallery-nav-toleft"><i class="fas fa-long-arrow-alt-left"></i></a>
                        <a href="#" class="gallery-nav-toright"><i class="fas fa-long-arrow-alt-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section" id="calc">
        <div class="container">
            <h2>Рассчитайте стоимость выбранной квартиры</h2>
            <div class="banks-list row">
                <div class="col-6 col-sm-4 col-lg-2"><img src="<?php echo ASSETS ?>/images/bank-logo-1.png" alt="Сбербанк"></div>
                <div class="col-6 col-sm-4 col-lg-2"><img src="<?php echo ASSETS ?>/images/bank-logo-2.png" alt="ВТБ-24" style="bottom:4px;"></div>
                <div class="col-6 col-sm-4 col-lg-2"><img src="<?php echo ASSETS ?>/images/bank-logo-3.png" alt="Дельтакредит"></div>
                <div class="col-6 col-sm-4 col-lg-2"><img src="<?php echo ASSETS ?>/images/bank-logo-4.png" alt="Уралсиб-банк" style="bottom:4px;"></div>
                <div class="col-6 col-sm-4 col-lg-2"><img src="<?php echo ASSETS ?>/images/bank-logo-5.png" alt="Россельхозбанк" style="bottom:4px;"></div>
                <div class="col-6 col-sm-4 col-lg-2"><img src="<?php echo ASSETS ?>/images/bank-logo-6.png" alt="Абсолют Банк" style="bottom:4px;"></div>
            </div>
            <div class="calc-wrapper">
                <div class="calc-sliders">
                    <div class="calc-param">
                        <div class="row">
                            <div class="col-md-6 calc-label">
                                Сумма кредита<br><span class="description">Выберите сумму</span>
                            </div>
                            <div class="col-md-6 text-right calc-value"><span>2 000 000</span> руб.</div>
                        </div>
                        <input type="text" id="calc-summ" name="summ" value="" />
                    </div>
                    <div class="calc-param">
                        <div class="row">
                            <div class="col-md-6 calc-label">
                                Срок кредита, месяцев<br><span class="description">Введите количество месяцев</span>
                            </div>
                            <div class="col-md-6 text-right calc-value"><span>240</span> месяцев</div>
                        </div>
                        <input type="text" id="calc-period" name="period" value="" />
                    </div>
                    <div class="calc-param">
                        <div class="row">
                            <div class="col-md-6 calc-label">
                                Процентная ставка по кредитному договору, %<br><span class="description">Введите значение</span>
                            </div>
                            <div class="col-md-6 text-right calc-value"><span>9</span> %</div>
                        </div>
                        <input type="text" id="calc-percent" name="percent" value="" />
                    </div>
                </div>
                <div class="calc-result">
                    Размер ежемесячного платежа*
                    <div class="result"><span>18 909</span> руб.</div>
                    <small style="margin-top:5px;font-size:10px;text-align: left;display: block;line-height:1.5;">* Данный расчет является приблизительным, за более подробной информацией, обращайтесь к менеджеру банка. </small>
                    <!-- <a href="#callme" data-fancybox data-src="#callme" class="btn btn-primary btn-lg btn-block">Отправить рассчитать </a> -->
                </div>
            </div>
        </div>
    </div>

    <div class="section" id="feedback-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <h2>Остались вопросы?</h2>
                    <p class="lead">Получите бесплатную консультацию от&nbsp;наших специалистов</p>
                </div>
                <div class="col-lg-7">
                    <div class="form-container">
                        <?php echo do_shortcode('[contact-form-7 id="4" title="Форма вопрос пользователя"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php 
    $phone = get_field('phone', 'option');
        if($phone) {
            $normalized_phone = preg_filter('/[\D+]/i', '', $footer_phone);
        }
    ?>
    <div class="section" id="contacts">
        <div id="contacts-map">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ace5294df497425864c09c61e26a95f0c4582050ea075c0ba290371dc7e9c1542&amp;width=100%25&amp;height=720&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-8 col-md-6 col-sm-6">
                        <?php the_field('copyright', 'option'); ?>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 text-md-right text-sm-left">
                        <a href="tel:<?php echo $normalized_phone; ?>" class="footer__phone"><?php echo $phone; ?></a><br>
                        Отдел продаж
                    </div>
                </div>
                <div class="row footer__links-row">
                    <div class="col-xl-9 col-lg-8 col-md-6 col-sm-6">
                        <ul class="footer__menu">
                            
                            <?php if ( have_rows('footer_menu', 'option') ) : ?>
                                <?php while( have_rows('footer_menu', 'option') ) : the_row(); ?>

                                    <?php the_sub_field('sub_field_name'); ?>
                                    <li>
                                        <a target="_blank" <?php echo get_sub_field('attrs'); ?> href="<?php the_sub_field('url'); ?>"><?php the_sub_field('text'); ?></a>
                                    </li>
           
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 text-md-right text-sm-left">
                        <a data-src="#callme" data-fancybox href="#callme">Позвоните нам бесплатно</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>

</div>

</div> 

<?php get_footer(); ?>



<?php wp_footer(); ?>

<!--modals-->
<div id="callme" class="modal">
    <div class="form-container">
        <?php echo do_shortcode('[contact-form-7 id="119" title="Контактная форма"]'); ?>
    </div>
</div>

<div id="complaint" class="modal">
    <div class="form-container">
        <?php echo do_shortcode('[contact-form-7 id="126" title="Форма для жалоб и предложений"]'); ?>
    </div>
</div>

<style>
    #video {
        background-color: transparent;
    }

    #video video {
        margin: auto;
        display: block;
        max-width: 80%;
    }
</style>

<div id="video" style="display:none; max-width: 800px;padding:0;overflow:hidden;width:100%;">
    <div class="row">
        <div class="col-12">
            <video controls>
                <source src="<?php echo get_field('about_video_mp4')['url']; ?>" type="video/mp4">
                <?php if(get_field('about_video_webm')): ?>
                    <source src="<?php echo get_field('about_video_webm')['url']; ?>" type="video/webm">
                <?php endif; ?>
            </video>
        </div>
    </div>
</div>

</body>

</html>
const {
    lstatSync,
    readdirSync
} = require('fs')
const {
    join,
    basename
} = require('path')

const isDirectory = source => lstatSync(source).isDirectory()

/**
 * Get all subdirectories basenames of the source directory 
 * @param {string} source
 * @return {Array}
 */
const getDirectoriesBasenames = source => readdirSync(source)
    .map(name => join(source, name))
    .filter(isDirectory)
    .map(dir => basename(dir))

var path = require('path')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

const vueCssLoaders = function (options) {
    options = options || {}

    var cssLoader = {
        loader: 'css-loader',
        options: {
            minimize: process.env.NODE_ENV.trim() === 'production',
            sourceMap: options.sourceMap
        }
    }

    // generate loader string to be used with extract text plugin
    function generateLoaders(loader, loaderOptions) {
        var loaders = [cssLoader]
        if (loader) {
            loaders.push({
                loader: loader + '-loader',
                options: Object.assign({}, loaderOptions, {
                    sourceMap: options.sourceMap
                })
            })
        }

        // Extract CSS when that option is specified
        // (which is the case during production build)
        if (options.extract) {
            return ExtractTextPlugin.extract({
                use: loaders,
                fallback: 'vue-style-loader'
            })
        } else {
            return ['vue-style-loader'].concat(loaders)
        }
    }

    // https://vue-loader.vuejs.org/en/configurations/extract-css.html
    return {
        css: generateLoaders(),
        postcss: generateLoaders(),
        less: generateLoaders('less'),
        sass: generateLoaders('sass', {
            indentedSyntax: true
        }),
        scss: generateLoaders('sass'),
        stylus: generateLoaders('stylus'),
        styl: generateLoaders('stylus')
    }
}

module.exports = {
    isDirectory,
    getDirectoriesBasenames,
    vueCssLoaders 
}
<?php
/**
 * functions.php
 *
 * The theme's functions and definitions.
 */

define( 'THEMEROOT', get_stylesheet_directory_uri() );
define( 'ASSETS', get_stylesheet_directory_uri() . '/dist' );

if(! function_exists('moisland_acf_init') ) {
        function moisland_acf_init() {
            acf_update_setting('google_api_key', 'AIzaSyApiNU1iA_2rqHx5oqcrxtP9NyLziK0ruc');
        }
    
        add_action('acf/init', 'moisland_acf_init');
    }
    
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'title' => 'Общие поля',
        'slug' => 'common_fields',
    ));
}

if (! function_exists( 'moisland_assets' )) {
    function moisland_assets()
    {
        wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;subset=cyrillic' );
        wp_enqueue_style( 'bootstarp', ASSETS . '/css/bootstrap/bootstrap.min.css' );
        wp_enqueue_style( 'fa-reg', ASSETS . '/css/font-awesome/css/fa-regular.css' );
        wp_enqueue_style( 'fa-solid', ASSETS . '/css/font-awesome/css/fa-solid.css' );
        wp_enqueue_style( 'fa-brands', ASSETS . '/css/font-awesome/css/fa-brands.css' );
        wp_enqueue_style( 'fa', ASSETS . '/css/font-awesome/css/fontawesome.css' );
        wp_enqueue_style( 'fullpage-css', ASSETS . '/js/vendor/fullpage/jquery.fullpage.min.css' );
        wp_enqueue_style( 'ion.rangeslider-css', ASSETS . '/js/vendor/ion.rangeslider/css/ion.rangeSlider.css' );
        wp_enqueue_style( 'ion.rangeslider-skin', ASSETS . '/js/vendor/ion.rangeslider/css/ion.rangeSlider.skinNice.css' );
        wp_enqueue_style( 'fancybox-css', ASSETS . '/js/vendor/fancybox/jquery.fancybox.min.css' );
        wp_enqueue_style( 'slick-css', ASSETS . '/js/vendor/slick-slider/slick.css' );
        wp_enqueue_style( 'jquery.scrollbar-css', ASSETS . '/js/vendor/jquery.scrollbar/jquery.scrollbar.css' );
        wp_enqueue_style( 'main-css', ASSETS . '/css/main.css' );
        wp_enqueue_style( 'bundle-css', ASSETS . '/index.css' );

        wp_deregister_script('jquery'); // we have jquery bundled in vendor

        wp_register_script( 'jquery', ASSETS . "/js/vendor/jquery-3.2.1.min.js", array(), false, true );
        wp_register_script( 'bootstarp-js', ASSETS . "/js/vendor/bootstrap/bootstrap.min.js", array('jquery'), false, true );
        wp_register_script( 'fullpage', ASSETS . "/js/vendor/fullpage/jquery.fullpage.min.js", array(), false, true );
        wp_register_script( 'jquery.mask', ASSETS . "/js/vendor/jquery.mask/jquery.mask.min.js", array('jquery'), false, true );
        wp_register_script( 'ion.rangeSlider', ASSETS . "/js/vendor/ion.rangeslider/js/ion.rangeSlider.min.js", array('jquery'), false, true );
        wp_register_script( 'fancybox', ASSETS . "/js/vendor/fancybox/jquery.fancybox.min.js", array('jquery'), false, true );
        wp_register_script( 'slick', ASSETS . "/js/vendor/slick-slider/slick.min.js", array('jquery'), false, true );
        wp_register_script( 'jquery.scrollbar', ASSETS . "/js/vendor/jquery.scrollbar/jquery.scrollbar.min.js", array('jquery'), false, true );
        wp_register_script( 'main-js', ASSETS . "/js/main.js", array('jquery'), false, true );
        
        wp_register_script( 'bundle-js', ASSETS . "/index.js", array(), false, true );

        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'bootstarp-js' );
        wp_enqueue_script( 'fullpage' );
        wp_enqueue_script( 'jquery.mask' );
        wp_enqueue_script( 'ion.rangeSlider' );
        wp_enqueue_script( 'fancybox' );
        wp_enqueue_script( 'slick' );
        wp_enqueue_script( 'jquery.scrollbar' );
        wp_enqueue_script( 'main-js' );

        // Localize the script with new data
        $path_array = array(
            'assetsUrl' => ASSETS,
            'ajax'  => admin_url( 'admin-ajax.php' )
        );
        $shortcodes = array();
        $shared_data = array();
        wp_localize_script( 'bundle-js', 'wpPaths', $path_array );
        wp_localize_script( 'bundle-js', 'wpShortcodes', $shortcodes );
        wp_localize_script( 'bundle-js', 'wpSharedData', $shared_data );

        wp_enqueue_script( 'bundle-js' );
    }
    add_action( 'wp_enqueue_scripts', 'moisland_assets', 0 );
}

add_filter('wpcf7_autop_or_not', '__return_false');

/**
 * Fix svg in media library 
 */
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {
    
      global $wp_version;
      if ( $wp_version !== '4.7.1' ) {
         return $data;
      }
    
      $filetype = wp_check_filetype( $filename, $mimes );
    
      return [
          'ext'             => $filetype['ext'],
          'type'            => $filetype['type'],
          'proper_filename' => $data['proper_filename']
      ];
    
    }, 10, 4 );
    
function moisland_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'moisland_mime_types' );

function moisland_fix_svg() {
    echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
                width: 100% !important;
                height: auto !important;
        }
        </style>';
}
add_action( 'admin_head', 'moisland_fix_svg' );


add_action( 'wp_ajax_get_apartments', 'moisland_ajax_get_apartments' );
add_action( 'wp_ajax_nopriv_get_apartments', 'moisland_ajax_get_apartments' );

function moisland_ajax_get_apartments() {
    ob_start();

    $response = wp_remote_get( 'http://www.mois1.ru/ajax/flats_json2.php' ); 
    wp_send_json( json_decode($response['body'], true) );

     die();
 }

add_action( 'wp_ajax_get_houses', 'moisland_ajax_get_houses' );
add_action( 'wp_ajax_nopriv_get_houses', 'moisland_ajax_get_houses' );

function moisland_ajax_get_houses() {
    ob_start();

    $response = wp_remote_get( 
        'http://www.mois1.ru/ajax/for_curl2.php'
    ); 
    wp_send_json( json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response['body']), true) );

    die();
 }

 add_action( 'wp_ajax_get_floor', 'moisland_ajax_get_floor' );
 add_action( 'wp_ajax_nopriv_get_floor', 'moisland_ajax_get_floor' );
 
 function moisland_ajax_get_floor() {
     ob_start();
 
     $SECTION_ID = $_POST['id'];
     $response = wp_remote_get( 
         'http://www.mois1.ru/ajax/for_curl2.php?SECTION_ID=' . $SECTION_ID 
     ); 
     wp_send_json( json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response['body']), true) );
 
     die();
 }

 add_action( 'wp_ajax_get_house', 'moisland_ajax_get_house' );
 add_action( 'wp_ajax_nopriv_get_house', 'moisland_ajax_get_house' );
 
 function moisland_ajax_get_house() {
     ob_start();
 
     $SECTION_ID = $_POST['id'];
     $response = wp_remote_get( 
         'http://www.mois1.ru/ajax/for_curl2.php?SECTION_ID=' . $SECTION_ID 
     ); 
     wp_send_json( json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response['body']), true) );
 
     die();
 }

add_action( 'wp_ajax_get_apartment', 'moisland_ajax_get_apartment' );
add_action( 'wp_ajax_nopriv_get_apartment', 'moisland_ajax_get_apartment' );

function moisland_ajax_get_apartment() {
    ob_start();

    $ELEMENT_ID = $_POST['id'];
    $response = wp_remote_get( 
        'http://www.mois1.ru/ajax/for_curl2.php?ELEMENT_ID=' . $ELEMENT_ID  
    ); 
    wp_send_json( json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response['body']), true) );

    die();
}
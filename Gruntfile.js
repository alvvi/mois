module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            default: {
                options: { // Target options
                    style: 'expanded'
                },
                files: { // Dictionary of files
                    'public/css/main.css': 'public/css/sass/main.scss'
                }
            }
        },
        watch: {
            html: {
                files: '/public/index.html',
                tasks: [],
                options: {
                    livereload: true
                },
            },
            sass: {
                files: 'public/css/sass/*.scss',
                tasks: ['sass'],
                options: {
                    livereload: true,
                    debounceDelay: 50,
                },
            },
        },
    })
    grunt.loadNpmTasks('grunt-contrib-sass')
    grunt.loadNpmTasks('grunt-contrib-watch')

    grunt.registerTask('default', ['sass'])
}
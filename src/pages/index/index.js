import '@/js/polyfils'
import Vue from 'vue'
import Tabs from 'Components/Tabs/Tabs'
import ApartmentTable  from 'Components/ApartmentTable/ApartmentTable.vue'
import Loader  from 'Components/Loader/Loader.vue'
import {
    EventBus
} from 'Components/event-bus'
import {
    getApartments,
    getHouses,
    getApartment,
    getFloor,
    getHouse
} from '@/js/api'
import {
    arrayToHash
} from '@/js/utils/hashTable'

const $ = window.jQuery || window.$
Vue.config.devtools = true

Vue.component('apartment-table', ApartmentTable)
Vue.component('loader', Loader)

const App = new Vue({
    el: '#App',
    data: {
        apartmentTableVisible: false,
        apartmentTableLoading: false,
        apartments: {
            byId: {},
        },
        houses: {
            byId: {}
        },
        floors: {
            byId: {}
        },
        tooltip: {
            content: 'Привет',
            top: 0,
            left: 0,
            visible: false,
            class: ''
        },
        activeFloor: 0,
        activeApartment: {},
        floorDetailsOpened: false,
        floorDetailsLoading: false,
        apartmentDetailsOpened : false,
        apartmentDetailsLoading: false,
        activeHouseId: 0,
        activeTab: 'tab-interactive',
        houseLoading: true
    },
    methods: {
        closeAllModals() {
            Object.keys(this.modals).forEach(name => this.closeModal(name))
        },
        closeModal(name) {
            this.modals[name].visible = false
            $('body').css('overflow', 'auto')
        },
        openModal(name) {
            this.closeAllModals()

            this.modals[name].visible = true
            $('body').css('overflow', 'hidden')
        },
        showApartmentTable() {
            this.apartmentTableVisible = true 
        },
        hideApartemntTable() {
            this.apartmentTableVisible = false 
        },
        showTab(id) {
            this.activeTab = id 
        },

        houseTooltipContent(activeHouse, floor) {
            if( this.isHouseFloorSold(activeHouse, floor) ) {
                return `Этаж ${floor.number}`
            }

            return 'Этаж не в продаже'
        },

        isHouseFloorSold(activeHouse, floor) {
            const floorData = activeHouse.floors[floor.url]

            return floorData 
            && floorData.FLATS_FOR_SALE
            && floorData.FLATS_FOR_SALE.length > 0 
        },


        addFloor(data) {
            this.$set(this.floors.byId, data.FLOOR.ID, {
                id: data.FLOOR.ID,
                img: data.FLOOR.PICTURE,
                name: data.FLOOR.NAME,
                flats: data.FLATS.length > 0 ? arrayToHash(data.FLATS, 'CODE') : undefined,
                coordinates: data.FLOOR.COORDINATES,
                code: data.FLOOR.CODE,
                house: data.HOUSE.ID,
                width: data.FLOOR.VWIDTH ? data.FLOOR.VWIDTH : 720,
                height: data.FLOOR.VHEIGHT ? data.FLOOR.VHEIGHT : 720
            })
            this.activeFloor = data.FLOOR.ID  
        },

        addHouse(data) {
            console.log(data)
            const id = data.HOUSE.ID
            const floors = data.FLOORS

            const house = Object.assign({}, this.houses.byId[id], {
                coordinates: data.HOUSE.COORDINATES,
                floors: arrayToHash(data.FLOORS, 'CODE'),
                img: data.HOUSE.PICTURE
            })

            this.$set(this.houses.byId, id, house) 
            return id
        },

        addHouses(data) {
            const mappedHouses = data.HOUSES.map(house => ({
                name: house.NAME,
                //coordinates: house.COORDINATES,
                id: house.ID,
                //floors: arrayToHash(house.FLOORS, 'CODE'),
                code: house.CODE
            }))
            const allHouses = Object.assign({}, this.houses.byId, arrayToHash(mappedHouses, 'id'))
            this.$set(this.houses, 'byId', allHouses)

            const buildings = $(data.PLAN).find('[data-buildings]').data('buildings') 
            buildings.forEach(data => {
                const house = this.housesArray.filter(house => `${house.code}/` === data.url)[0]
                
                if(house) {
                    if(this.houses.byId[house.id]) 
                        this.$set(this.houses.byId[house.id], 'avail', data.avail)
                }
            })
        },

        addApartments(apartments) {
            const mappedApartments = apartments.map(apartment => {
                const price = parseInt(apartment.PRICE_SUM)
                const price_special = parseInt(apartment.SALE_FULL_PRICE)

                return {
                    id: apartment.ELEMENT_ID, 
                    acf: {
                        apartment_address: apartment.HOUSE.replace('проспект', 'пр.'),  
                        apartment_info: {
                            price: isNaN(price) ? 0 : price,
                            price_special: isNaN(price_special) ? 0 : price_special, 
                            area: parseFloat(apartment.SQUARE.replace(',', '.')),
                            rooms: parseInt(apartment.ROOMS),
                            floor: parseInt(apartment.FLOOR),
                            number: parseInt(apartment.NAME),
                            status: apartment.STATUS,
                            house: apartment.HOUSE, 
                            houseNumber: apartment.HOUSE.replace( /^\D+/g, '')
                        }
                    }
                }
            })
            const allApartments = Object.assign({}, this.apartments.byId, arrayToHash(mappedApartments, 'id'))
            this.$set(this.apartments, 'byId', allApartments)
        },

        handleHousePathMouseEnter(e, content) {
            const $target = $(e.target)
            this.tooltip = Object.assign({}, this.tooltip, {
                visible: true,
                content,
                top: $target.position().top,
                left: $target.position().left,
                class: 'tooltip--center'
            }) 
        },

        handleHousePathMouseLeave() {
            this.tooltip = Object.assign({},this.tooltip, {
                visible: false,
                content: '',
                top: 0,
                left: 0,
                class: ''
            })
        },

        handleFloorPathMouseEnter(e, content) {
            const $target = $(e.target)
            this.tooltip = Object.assign({}, this.tooltip, {
                visible: true,
                content,
                top: $target.position().top,
                left: $target.position().left 
            }) 
        },

        handleFloorPathMouseLeave() {
            this.tooltip = Object.assign({},this.tooltip, {
                visible: false,
                content: '',
                top: 0,
                left: 0
            })
        },

        isHouseAvaliable(id) {
            return this.houses.byId[id] && this.houses.byId[id].avail !== false
        },

        openHouseDetails(houseId) {
            const house = this.houses.byId[houseId]

            
            if(house && house.coordinates) {
                this.activeHouseId = houseId 
                this.activeTab = 'tab-house'
                return
            } 

            // if(house && !house.avail) {
            //     return 
            // }
            
            this.activeTab = 'tab-house'
            this.houseLoading = true 
            
            getHouse(houseId)
            .then(result => {
                if(result.status !== 200) return false 
                const id = this.addHouse(result.data)
                this.houseLoading = false
                this.activeHouseId = id 
            })
            .catch(() => {
                this.houseLoading = false
                this.activeTab = 'tab-interactive'
            })
            
        },

        openFloorDetails(houseId, code) {
            const floor = this.houses.byId[houseId].floors[code]
            if(floor == undefined) return false 
            const floorId = floor.ID

            if(!this.floors.byId[floorId]) { 
                this.floorDetailsLoading = true 

                getFloor(floorId)
                .then(result => {
                    if(result.status === 200) {
                        this.addFloor(result.data)
                        this.floorDetailsLoading = false
                    }
                })
                .catch(() => {
                    this.floorDetailsLoading = false
                })

            } else {
                this.activeFloor = floorId
            }
            
            this.floorDetailsOpened = true
        },

        showFloorView() {
            this.closeApartmentDetails()
            this.floorDetailsOpened = true 

            if(!this.floors.byId[this.activeFloor].flats) {
                this.floorDetailsLoading = true 
                getFloor(this.activeFloor)
                .then(result => {
                    if(result.status === 200) {
                        this.addFloor(result.data)
                        this.floorDetailsLoading = false
                    }
                })
            }

        },

        closeFloorDetails() {
            this.floorDetailsOpened = false 
        },

        updateApartment(data) {
            this.$set(this.activeApartment, 'img', data.FLAT.PICTURE) 
        },

        openApartmentDetails(floorId, code) {
            const apartment = this.floors.byId[floorId].flats[code]
            const apartmentId = apartment ? apartment.ID : false
            if(apartmentId === false || !this.apartments.byId[apartmentId]) return false 

            if(this.apartments.byId[apartmentId] && !this.apartments.byId[apartmentId].img) { 
                this.apartmentDetailsLoading = true 
                getApartment(apartmentId)
                .then(result => {
                    if(result.status === 200) {
                        this.updateApartment(result.data)
                        this.apartmentDetailsLoading = false
                    }
                })
            }
            
            this.activeApartment = this.apartments.byId[apartmentId]
            this.apartmentDetailsOpened = true
        },

        closeApartmentDetails() {
            this.apartmentDetailsOpened = false
        },

        changeFloor(floor) {
            const floorId = floor.ID

            if(!floorId) return false

            if(!this.floors.byId[floorId]) {
                this.floorDetailsLoading = true 

                getFloor(floorId)
                    .then(result => {
                        if(result.status === 200) {
                            this.addFloor(result.data)
                            this.floorDetailsLoading = false
                        }
                    })

            } else {
                this.activeFloor = floorId
            }
        },

        showSingleApartment(apartmentId) {
            if(this.apartments.byId[apartmentId] && !this.apartments.byId[apartmentId].img) { 
                this.apartmentDetailsLoading = true 
                getApartment(apartmentId)
                .then(result => {
                    if(result.status === 200) {
                        this.updateApartment(result.data)
                        this.addFloor(result.data)
                        this.apartmentDetailsLoading = false
                    }
                })


            }

            this.activeApartment = this.apartments.byId[apartmentId]
            this.apartmentDetailsOpened = true
        }
    },
    computed: {
        apartmentsArray() {
            return Object.values(this.apartments.byId)
        },
        housesArray() {
            return Object.values(this.houses.byId)
        },
        isMobile() {
            return $(window).width() < 1024 
        },
        activeHouse() {
            return this.houses.byId[this.activeHouseId] ? this.houses.byId[this.activeHouseId] : false 
        }, 
        nextFloor() {
            const activeFloor = this.floors.byId[this.activeFloor]

            if(!activeFloor) return false 
            if(!this.houses.byId[activeFloor.house]) return false
            
            const nextFloorNumber = +activeFloor.code + 1

            if(isNaN(nextFloorNumber)) {
                const code = String(activeFloor.code)
                const entrance = code.match(/\d+-/)
                const number = +code.replace(/\d+-/, '') + 1
                return this.houses.byId[activeFloor.house].floors[entrance + number] || false
            }

            return this.houses.byId[activeFloor.house].floors[String(nextFloorNumber)] || false
        },
        prevFloor() {
            const activeFloor = this.floors.byId[this.activeFloor]

            if(!activeFloor) return false
            if(!this.houses.byId[activeFloor.house]) return false
            
            const prevFloorNumber = +activeFloor.code - 1 

            if(isNaN(prevFloorNumber)) {
                const code = String(activeFloor.code)
                const entrance = code.match(/\d+-/)
                const number = +code.replace(/\d+-/, '') - 1
                return this.houses.byId[activeFloor.house].floors[entrance + number] || false
            }

            return this.houses.byId[activeFloor.house].floors[String(prevFloorNumber)] || false
        }
    },
    created() {

        this.apartmentTableLoading = true
        getApartments()
            .then(result => {
                if (result.status === 200) {
                    this.addApartments(result.data)
                }
                this.apartmentTableLoading = false
            })
            .catch(e => {
                this.apartmentTableLoading = false
            })

        getHouses()
            .then(result => {
                if (result.status === 200) {
                    this.addHouses(result.data)
                }
            })

    },
    mounted() {

        EventBus.$on('showSingleApartment', id => this.showSingleApartment(id))

        $(() => {

            if($(window).width() < 1024) {
                this.showApartmentTable()
            }

            $('[data-reflect]').on('change', e => {
                const $target = $(e.currentTarget)
                const name = $target.data('reflect')
                const val = $target[0].checked
                const $checkbox = 
                    $target
                    .closest('.wpcf7')
                    .find(`[name="${name}[]"]`)[0].checked = val 
            })
            
            $('[data-reflect]').each((i, el) => {
                const $target = $(el)
                const name = $target.data('reflect')
                $target
                    .closest('.wpcf7')
                    .on('wpcf7submit', () => {
                        const isInvalid = $target
                            .closest('form').find(`[name="${name}[]"]`)
                            .parent().parent()
                            .hasClass('wpcf7-not-valid')
                        
                        isInvalid ? $target.addClass('invalid') : $target.removeClass('invalid')
                    })
            })

            $('[name="userphone"]').mask('+7 (000) 000 00 00', {
                placeholder: '+7 (___) ___ __ __'
            })

            $('#contacts-map').append('<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ace5294df497425864c09c61e26a95f0c4582050ea075c0ba290371dc7e9c1542&amp;width=100%25&amp;height=720&amp;lang=ru_RU&amp;scroll=false"></script>')
        })
        
    }
})

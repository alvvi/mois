
/**
 * Converts an array of objects to a single hash with second argument property as keys,
 * if an object doesn't have that property, skips it
 * @param {Array} arr Array to convert 
 * @param {String} prop Prop to be used as hash-key 
 * @return {Object}
 */
export function arrayToHash(arr, prop) {

    if(arr.length !== undefined && arr.reduce) {
    
        const hash = arr.reduce((acc, curr) => {

            if(!curr.hasOwnProperty(prop)) return acc

            acc[curr[prop]] = curr
            return acc 
        }, {})

        return hash

    }

    return arr 
}
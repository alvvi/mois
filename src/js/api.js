const $ = window.jQuery || window.$

const $post = (url, data) => {
    return new Promise((resolve, reject) => {
        $
        .post(url, data)
        .done((data, status, response) => {
            resolve({
                data,
                status: response.status,
                code: status  
            }) 
        })
        .fail(() => {
            reject()
        })
    })
}


export function getApartments() {
    return $post(window.wpPaths.ajax, {
        action: 'get_apartments',
    })
}

export function getHouses() {
    return $post(window.wpPaths.ajax, {
        action: 'get_houses'
    })
}

export function getHouse(id) {
    return $post(window.wpPaths.ajax, {
        action: 'get_house',
        id
    })
}

export function getFloor(id) {
    return $post(window.wpPaths.ajax, {
        action: 'get_floor',
        id 
    })
}

export function getApartment(id) {
    return $post(window.wpPaths.ajax, {
        action: 'get_apartment',
        id 
    })
}
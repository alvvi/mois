const $ = window.jQuery || window.$ 

$('input[type=tel]').mask('+7 (999) 999-99-99')
$('input[type=tel]').mask('+7 (999) 999-99-99')

$('[data-reflect]').on('change', e => {
    const $target = $(e.currentTarget)
    const name = $target.data('reflect')
    const val = $target[0].checked
    const $checkbox = 
        $target
        .closest('.wpcf7')
        .find(`[name="${name}[]"]`)[0].checked = val 
})

$('[data-reflect]').each((i, el) => {
    const $target = $(el)
    const name = $target.data('reflect')

    $target
        .closest('.wpcf7')
        .on('wpcf7invalid', () => {
            const isInvalid = $target
                .closest('form').find(`[name="${name}[]"]`)
                .parent().parent()
                .hasClass('wpcf7-not-valid')
        
            isInvalid ? $target.addClass('invalid') : $target.removeClass('invalid')
        })
})
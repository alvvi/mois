const $ = window.jQuery || window.$ 

class Tabs {
    constructor(options) {

        if(options.controls) {

            if(typeof options.controls === 'string') {
                this._controls = $(options.controls)
            } else {
                this._controls = options.controls 
            }

        } else {
            this._controls = $()
        }

        if(options.tabs && $(options.tabs).length > 0) {

            if(typeof options.tabs === 'string') {
                this._tabs = $(options.tabs).children()
            } else {
                this._tabs = options.tabs.children()
            }

        }

        this._classes = {
            tabActive: options.tabActiveClass || false,
            controlActive: options.controlActiveClass || false, 
        }

        const guessCurrent = this._tabs.filter('[data-tab-current]') 
        const currentExists = guessCurrent.length > 0 
 
        const currentTab = currentExists ? guessCurrent : this._tabs.eq(0) 
        currentTab.attr('data-tab-current', '')  
        const currentTabIndex = currentTab.attr('data-tab-index')

        this.state = { currentTab: currentTabIndex, options }
        this.changeCurrent(this.state.currentTab)
        this.attachListeners()
    }

    attachListeners() {
        this._controls.on('click', (e) => {
            e.preventDefault()
            const $target = $(e.target)
            const hasIndex = $target.attr('data-tab-index')

            const control = hasIndex ? $target : $target.closest('data-tab-index')
            const tabIndex = control.attr('data-tab-index')

            this.changeCurrent(tabIndex)
        })
    }

    getCurrent() {
        return String( this.state.currentTab )  
    }

    changeCurrent(index) {
        this.state.currentTab = index
        this.updateControls()
        this.updateTab()
    }

    updateControls() {
        const next = this._controls.filter(`[data-tab-index="${this.state.currentTab}"]`)
        if(next.length <= 0) return false

        const activeClass = this._classes.controlActive || false
        if(activeClass) {
            this._controls.removeClass(activeClass)
            next.addClass(activeClass)
        }
    }

    updateTab() {
        const $active = this._tabs.filter('[data-tab-current]')
        const activeClass = this._classes.tabActive || false 

        const $next = this._tabs.filter(`[data-tab-index="${this.state.currentTab}"]`)
        if($next.length <= 0) return false

        if(this.state.options.setHeight) {
            this._tabs.parent().height($next.height())
        }

        if(activeClass) {
            $active.removeClass(activeClass)
            $next.addClass(activeClass)
        }

        this._tabs.removeAttr('data-tab-current')
        this._tabs.attr('aria-hidden', 'true')

        $next.attr('data-tab-current', 'true')
        $next.attr('aria-hidden', 'false')

        if(this.state.options.onTabUpdate) {
            this.state.options.onTabUpdate($active, $next) 
        }
    }
}

export default Tabs 
